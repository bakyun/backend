package redis

import (
	"context"
	"strings"
	"time"

	"github.com/gomodule/redigo/redis"
	"github.com/rs/zerolog"
	"gitlab.com/paars/bakyun/backend/config"
	"go.uber.org/fx"
)

const (
	defaultPort          = ":6379"
	maxIdleConnections   = 10
	maxActiveConnections = 100
	maxIdleTimeout       = 30 * time.Second
)

type redisOptions struct {
	Protocol string `default:"tcp"`
	Address  string `default:"localhost:6379"`
	Password string
	Database int
}

func Provider(lc fx.Lifecycle, cfg *config.Config, logger zerolog.Logger) *redis.Pool {
	log := logger.With().Str("module", "redis").Logger()
	pool := new(redis.Pool)

	fullAddr := cfg.Redis.Host
	if !strings.Contains(fullAddr, ":") {
		fullAddr += defaultPort
	}

	dialOpts := []redis.DialOption{
		redis.DialConnectTimeout(maxIdleTimeout),
		redis.DialDatabase(cfg.Redis.Database),
	}

	if cfg.Redis.Password != "" {
		dialOpts = append(dialOpts, redis.DialPassword(cfg.Redis.Password))
	}

	pool.Dial = func() (redis.Conn, error) {
		return redis.Dial("tcp", fullAddr, dialOpts...)
	}

	pool.MaxIdle = maxIdleConnections
	pool.MaxActive = maxActiveConnections
	pool.IdleTimeout = maxIdleTimeout
	pool.Wait = true

	lc.Append(fx.Hook{
		OnStart: func(c context.Context) error {
			log.Debug().Msg("connecting...")
			client := pool.Get()
			defer client.Close()

			_, err := client.Do("PING")
			return err
		},
		OnStop: func(c context.Context) error {
			log.Debug().Msg("disconnecting...")
			return pool.Close()
		},
	})

	return pool
}
