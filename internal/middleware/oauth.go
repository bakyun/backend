package middleware

import (
	"context"
	"net/http"

	"github.com/RangelReale/osin"
	"gitlab.com/paars/bakyun/backend/config"
)

// NewOAuth creates a new oauth server implementation
func NewOAuth(cfg *config.Config) (*osin.Server, error) {
	//manager.MustTokenStorage(store.New)
	serverConfig := osin.NewServerConfig()

	// add refresh token support
	serverConfig.AllowedAccessTypes = append(
		serverConfig.AllowedAccessTypes,
		osin.AUTHORIZATION_CODE,
		osin.REFRESH_TOKEN,
	)

	server := osin.NewServer(serverConfig, NewOAuthStorage(cfg))

	return server, nil
}

// AddOAuthToContext adds the given osin Server to a context
func AddOAuthToContext(ctx context.Context, oauthServer *osin.Server) context.Context {
	return context.WithValue(ctx, oauthKey, oauthServer)
}

// OAuthFromContext retrieves the osin Server from a context
func OAuthFromContext(ctx context.Context) *osin.Server {
	v := ctx.Value(oauthKey)
	if v == nil {
		return nil
	}

	return v.(*osin.Server)
}

// OAuthFromRequest retrieves the osin Server from a http.Request
func OAuthFromRequest(r *http.Request) *osin.Server {
	return OAuthFromContext(r.Context())
}

// OAuthMiddleware is a convinient middleware to add the osin Server to a handler middleware
func OAuthMiddleware(oauthServer *osin.Server) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			next.ServeHTTP(w, r.WithContext(AddOAuthToContext(r.Context(), oauthServer)))
		})
	}
}
