package util

import (
	"github.com/octago/sflags"
	"gopkg.in/urfave/cli.v2"
)

// FlagsGenerateTo takes a list of sflag.Flag,
// that are parsed from some config structure, and put it to dst.
func FlagsGenerateTo(src []*sflags.Flag, dst *[]cli.Flag) {
	for _, srcFlag := range src {
		name := srcFlag.Name
		var aliases []string
		if srcFlag.Short != "" {
			aliases = append(aliases, srcFlag.Short)
		}
		gflag := &cli.GenericFlag{
			Name:    name,
			Aliases: aliases,
			EnvVars: []string{srcFlag.EnvName},
			Hidden:  srcFlag.Hidden,
			Usage:   srcFlag.Usage,
			Value:   srcFlag.Value,
		}
		*dst = append(*dst, gflag)
	}
}

// FlagsParseTo parses cfg, that is a pointer to some structure,
// and puts it to dst.
func FlagsParseTo(cfg interface{}, dst *[]cli.Flag, optFuncs ...sflags.OptFunc) error {
	flags, err := sflags.ParseStruct(cfg, optFuncs...)
	if err != nil {
		return err
	}
	FlagsGenerateTo(flags, dst)
	return nil
}

// FlagsParse parses cfg, that is a pointer to some structure,
// puts it to the new flag.FlagSet and returns it.
func FlagsParse(cfg interface{}, optFuncs ...sflags.OptFunc) ([]cli.Flag, error) {
	flags := make([]cli.Flag, 0)
	err := FlagsParseTo(cfg, &flags, optFuncs...)
	if err != nil {
		return nil, err
	}
	return flags, nil
}

// FlagStringerFixer modifies the regular cli.FlagStringer, so that boolean generic flags
// are properly recognized and the value placeholder is hidden
func FlagStringerFixer() {
	oldStringify := cli.FlagStringer
	cli.FlagStringer = func(flag cli.Flag) string {
		if gflag, ok := flag.(*cli.GenericFlag); ok {
			if boolValue, ok := gflag.Value.(sflags.BoolFlag); ok && boolValue.IsBoolFlag() {
				getter, _ := boolValue.(sflags.Getter)
				val, _ := getter.Get().(bool)

				flag = &cli.BoolFlag{
					Name:        gflag.Name,
					Aliases:     gflag.Aliases,
					Usage:       gflag.Usage,
					EnvVars:     gflag.EnvVars,
					Hidden:      gflag.Hidden,
					Value:       val,
					DefaultText: gflag.DefaultText,
				}
			}
		}
		return oldStringify(flag)
	}
}
