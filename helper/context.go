package helper

import (
	"context"
	"net/http"

	"github.com/go-pg/pg"
	"github.com/mediocregopher/radix.v3"

	"github.com/gorilla/sessions"
)

type contextKey string

func (c contextKey) String() string {
	return "gitlab.com/paars/bakyun/helper." + string(c)
}

var (
	sessionStoreContextKey contextKey = "session-store"
	redisContextKey        contextKey = "redis"
	postgresContextKey     contextKey = "postgres"
)

// SessionStoreForContext returns the session store from the context
func SessionStoreForContext(ctx context.Context) sessions.Store {
	rawValue := ctx.Value(sessionStoreContextKey)
	if rawValue != nil {
		return rawValue.(sessions.Store)
	}

	return nil
}

// SessionStoreMiddleware adds the session store to the http.Request
func SessionStoreMiddleware(sessionStore sessions.Store) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), sessionStoreContextKey, sessionStore)))
		})
	}
}

// RedisForContext returns the session store from the context
func RedisForContext(ctx context.Context) radix.Client {
	rawValue := ctx.Value(redisContextKey)
	if rawValue != nil {
		return rawValue.(radix.Client)
	}

	return nil
}

// RedisMiddleware adds the session store to the http.Request
func RedisMiddleware(redis radix.Client) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), redisContextKey, redis)))
		})
	}
}

// PostgresForContext returns the session store from the context
func PostgresForContext(ctx context.Context) *pg.DB {
	rawValue := ctx.Value(postgresContextKey)
	if rawValue != nil {
		return rawValue.(*pg.DB)
	}

	return nil
}

// PostgresMiddleware adds the session store to the http.Request
func PostgresMiddleware(postgres pg.DB) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), postgresContextKey, postgres)))
		})
	}
}
