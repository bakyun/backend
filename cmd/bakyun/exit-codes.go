package main

// The various exit codes
const (
	ExitOK = iota
	ExitMissingConfigFile
	ExitHTTP
	ExitMisc

	ExitUnkownError = 255
)
