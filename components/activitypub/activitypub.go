package activitypub

import (
	"net/http"
	"net/url"
	"time"

	"gitlab.com/paars/bakyun/plugins"

	"github.com/go-pg/pg"
	"gitlab.com/paars/bakyun/backend/cacher"

	"github.com/go-fed/activity/deliverer"
	"github.com/rs/zerolog"

	"github.com/go-fed/activity/pub"
)

const backoffFactor = 1.5
const connectionTimeout = 30 * time.Second

// New returns a configured activity pubber and handler
func New(clock pub.Clock, app pub.FederateApplication, cb pub.Callbacker, deliverer *deliverer.DelivererPool, httpClient *http.Client, userAgent string, deliveryDepth, forwardingDepth int) (pub.Pubber, pub.HandlerFunc) {
	pubber := pub.NewFederatingPubber(
		clock, app, cb,
		deliverer, httpClient, userAgent,
		deliveryDepth, forwardingDepth,
	)

	handler := pub.ServeActivityPubObject(app, clock)

	return pubber, handler
}

// Defaults creates a pubber and handler with default configurations where possible
func Defaults(log zerolog.Logger, cacher *cacher.Cacher, pg *pg.DB, public *url.URL, fp plugins.FileProvider, userAgent string, deliveryRetries, deliveryDepth, forwardingDepth int) (pub.Pubber, pub.HandlerFunc) {
	clock := &Clock{}
	app := &Application{
		baseURL: public,
		log:     log.With().Str("part", "application").Logger(),
		cacher:  cacher,
		pg:      pg,
		fp:      fp,
	}
	cb := &Callbacker{}
	httpClient := *http.DefaultClient
	httpClient.Timeout = connectionTimeout

	deliverer := NewDeliverer(NewDeliveryPersister(), DefaultRateLimiter, deliveryRetries)

	return New(clock, app, cb, deliverer, &httpClient, userAgent, deliveryDepth, forwardingDepth)
}
