package postgres

import (
	"context"
	"strings"

	"github.com/rs/zerolog"

	"gitlab.com/paars/bakyun/backend/config"
	"go.uber.org/fx"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/jinzhu/inflection"
)

const defaultPort = ":5432"

type pgOptions struct {
	Network  string `default:"tcp"`
	Addr     string `default:"localhost:5432"`
	User     string
	Password string
	Database string
}

func Provider(lc fx.Lifecycle, cfg *config.Config, logger zerolog.Logger) *pg.DB {
	log := logger.With().Str("module", "pg").Logger()

	pgOptions := &pg.Options{
		Addr:     cfg.Postgres.Host,
		User:     cfg.Postgres.Username,
		Password: cfg.Postgres.Password,
		Database: cfg.Postgres.Database,
	}

	if !strings.Contains(pgOptions.Addr, ":") {
		pgOptions.Addr += defaultPort
	}

	orm.SetTableNameInflector(inflection.Singular)
	db := pg.Connect(pgOptions)

	lc.Append(fx.Hook{
		OnStart: func(c context.Context) error {
			log.Debug().Msg("connecting...")
			_, err := db.WithContext(c).ExecOne("SELECT 1")
			return err
		},
		OnStop: func(c context.Context) error {
			log.Debug().Msg("disconnecting...")
			return db.WithContext(c).Close()
		},
	})

	return db
}
