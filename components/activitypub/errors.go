package activitypub

import "errors"

// The different errors this package can throw
var (
	ErrUnkownObject = errors.New("unkown object")
)
