package activitypub

import (
	"golang.org/x/time/rate"
)

const (
	// DefaultRateLimit fills the bucket with 10 tokens per second
	DefaultRateLimit rate.Limit = 100

	// DefaultRateBucket limits the token bucket to max 100 tokens
	DefaultRateBucket int = 1000
)

var (
	// DefaultRateLimiter is the default rate limit instance
	DefaultRateLimiter = rate.NewLimiter(DefaultRateLimit, DefaultRateBucket)
)
