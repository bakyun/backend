package model

import "github.com/go-pg/pg"

// Application model for the application table
type Application struct {
	ID           int
	ClientID     string
	ClientSecret string
	Name         string
	Homepage     string
	RedirectURI  string
}

// ApplicationByClientID queries the database for an application by a client id
func ApplicationByClientID(db *pg.DB, clID string) (*Application, error) {
	m := new(Application)
	err := db.Model(m).Where("client_id = ?", clID).Select(m)
	if err != nil {
		return nil, err
	}

	return m, nil
}

// GetId returns the client id of the application
func (a *Application) GetId() string { //revive:disable-line:var-naming
	return a.ClientID
}

// GetSecret returns the client secret of the application
func (a *Application) GetSecret() string {
	return a.ClientSecret
}

// GetRedirectUri returns the redirect URI of the application
func (a *Application) GetRedirectUri() string { //revive:disable-line:var-naming
	return a.RedirectURI
}

// GetUserData is a nop for now
func (a *Application) GetUserData() interface{} {
	return nil
}
