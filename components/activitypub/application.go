package activitypub

import (
	"context"
	"crypto"
	"net/http"
	"net/url"
	"strings"

	"github.com/go-fed/activity/pub"
	"github.com/go-fed/activity/streams"
	"github.com/go-fed/activity/vocab"
	"github.com/go-fed/httpsig"
	"github.com/go-pg/pg"
	"github.com/rs/zerolog"
	"gitlab.com/paars/bakyun/backend/cacher"
	"gitlab.com/paars/bakyun/backend/model"
	"gitlab.com/paars/bakyun/plugins"
)

const (
	activityPubID = "id"
)

// Application is the activitypub application definition
type Application struct {
	log     zerolog.Logger
	baseURL *url.URL
	cacher  *cacher.Cacher
	pg      *pg.DB
	fp      plugins.FileProvider
}

// Owns checks if an id is owned by this application
func (a *Application) Owns(_ context.Context, id *url.URL) bool {
	a.log.Debug().Str("left", id.String()).Str("right", a.baseURL.String()).Msg("owns")
	return !id.IsAbs() || (id.Scheme == a.baseURL.Scheme && id.Host == a.baseURL.Host)
}

// Get returns the public definition of a id
func (a *Application) Get(_ context.Context, id *url.URL, rw pub.RWType) (pub.PubObject, error) {
	a.log.Debug().Str("url", id.String()).Bool("rw", bool(rw)).Msg("get")
	segments := strings.Split(id.Path, "/")

	// strip empty first result
	if segments[0] == "" {
		segments = segments[1:]
	}

	switch segments[0] {
	case "users":
		username := segments[1]

		rawModel, err := a.cacher.Fetch("model:"+username, func() (interface{}, error) {
			return model.AccountByName(a.pg, username)
		})
		if err != nil {
			a.log.Warn().Err(err).Msg("failed to query user!")
			return nil, err
		}

		model, ok := rawModel.(*model.Account)
		if !ok {
			return nil, ErrUnkownObject
		}

		return model.Actor(a.fp, a.baseURL)

	default:
		a.log.Debug().Msg("unkown resource to get")
		return nil, ErrUnkownObject
	}
}

func (a *Application) GetAsVerifiedUser(c context.Context, id *url.URL, authdUser *url.URL, rw pub.RWType) (pub.PubObject, error) {
	panic("GetAsVerifiedUser not implemented")
}

func (a *Application) Has(c context.Context, id *url.URL) (bool, error) {
	panic("Has not implemented")
}

func (a *Application) Set(c context.Context, o pub.PubObject) error {
	panic("Set not implemented")
}

func (a *Application) GetInbox(c context.Context, r *http.Request, rw pub.RWType) (vocab.OrderedCollectionType, error) {
	panic("GetInbox not implemented")
}

func (a *Application) GetOutbox(c context.Context, r *http.Request, rw pub.RWType) (vocab.OrderedCollectionType, error) {
	panic("GetOutbox not implemented")
}

func (a *Application) NewId(c context.Context, t pub.Typer) *url.URL {
	panic("NewId not implemented")
}

func (a *Application) GetPublicKey(c context.Context, publicKeyId string) (pubKey crypto.PublicKey, algo httpsig.Algorithm, user *url.URL, err error) {
	panic("GetPublicKey not implemented")
}

func (a *Application) CanAdd(c context.Context, o vocab.ObjectType, t vocab.ObjectType) bool {
	panic("CanAdd not implemented")
}

func (a *Application) CanRemove(c context.Context, o vocab.ObjectType, t vocab.ObjectType) bool {
	panic("CanRemove not implemented")
}

func (a *Application) OnFollow(c context.Context, s *streams.Follow) pub.FollowResponse {
	panic("OnFollow not implemented")
}

func (a *Application) Unblocked(c context.Context, actorIRIs []*url.URL) error {
	panic("Unblocked not implemented")
}

func (a *Application) FilterForwarding(c context.Context, activity vocab.ActivityType, iris []*url.URL) ([]*url.URL, error) {
	panic("FilterForwarding not implemented")
}

func (a *Application) NewSigner() (httpsig.Signer, error) {
	panic("NewSigner not implemented")
}

func (a *Application) PrivateKey(boxIRI *url.URL) (privKey crypto.PrivateKey, pubKeyId string, err error) {
	panic("PrivateKey not implemented")
}
