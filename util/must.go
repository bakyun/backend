package util

import (
	"net/url"
)

// MustURLParse panics if url.Parse errors
func MustURLParse(rawURL string) *url.URL {
	u, err := url.Parse(rawURL)
	if err != nil {
		panic(err)
	}
	return u
}
