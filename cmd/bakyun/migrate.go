package main

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/paars/bakyun/backend/internal/model"
	cli "gopkg.in/urfave/cli.v2"
)

func init() {
	app.Commands = append(app.Commands, migrateCommand)
}

var migrateCommand = &cli.Command{
	Name:        "migrate",
	Usage:       "Migrate the database layout",
	Description: `Migrate the database layout to the most recent version`,

	Action: func(ctx *cli.Context) error {
		oldV, newV, err := model.Migrate(cfg.MustDB())

		if err != nil {
			err = NewExitMessageError(ExitHTTP, "Error occured on listener").SetInnerError(err)
		} else {
			log.Info().Int64("old", oldV).Int64("new", newV).Msg("ran migrations")
		}

		return err
	},
}
