package config

import (
	"os"

	"github.com/creasty/defaults"
	yaml "gopkg.in/yaml.v2"
)

// Config parses the config file
type Config struct {
	Postgres struct {
		Host     string `yaml:"host"`
		Username string `yaml:"username"`
		Password string `yaml:"password"`
		Database string `yaml:"database"`
	} `yaml:"postgres"`

	Redis struct {
		Host     string `yaml:"host"`
		Password string `yaml:"password"`
		Database int    `yaml:"database"`
	} `yaml:"redis"`

	Store struct {
		Provider      string            `yaml:"provider"`
		Configuration map[string]string `yaml:"configuration"`
	} `yaml:"store"`

	HTTP struct {
		Listen string `default:"localhost:8848" yaml:"listen"`
		Public string `default:"http://localhost:8848" yaml:"public"`
		Static string `yaml:"static"`

		SSL struct {
			Cert string `yaml:"cert"`
			Key  string `yaml:"key"`
		} `yaml:"ssl"`
	} `yaml:"http"`

	/*
		UCrypt struct {
			Algorithm     string `default:"argon2" yaml:"algorithm"`
			Configuration string `default:"id,16,16384" yaml:"configuration"`
		} `flag:"-" yaml:"ucrypt"`

		Variables struct {
			InstanceName string `default:"bakyun~" yaml:"instance-name"`
		} `flag:"-"  yaml:"variables"`

		Cookie Cookie `yaml:"cookie" flag:"-"`

		ActivityPub ActivityPub `flag:"-" yaml:"ap"`
	*/
}

// New creates a new instance of config and sets its default flags
func New() *Config {
	cfg := new(Config)
	cfg.setDefaults()

	return cfg
}

func (c *Config) setDefaults() {
	defaults.Set(c)
}

// Load loads a YAML config file
func (c *Config) Load(file string) error {
	if file == "" {
		return os.ErrNotExist
	}

	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	dec := yaml.NewDecoder(f)
	dec.SetStrict(true)

	return dec.Decode(c)
}
