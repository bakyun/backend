package config

import (
	"github.com/creasty/defaults"
)

// Cookie holds the cookie configuration
type Cookie struct {
	Keys []CookieKeyPair `yaml:"keys"`
}

// CookieKeyPair is a single keypair for the cookie encryption
type CookieKeyPair struct {
	Authentication string `yaml:"authentication" default:"notsoseekret"`
	Encryption     string `yaml:"encryption,omitempty"`
}

// GetByteKeys converts all cookiekeypairs to [][]byte so securecookie can use them
func (c *Cookie) GetByteKeys() [][]byte {
	var keyPairs [][]byte

	for _, kp := range c.Keys {
		keyPairs = append(keyPairs, []byte(kp.Authentication))
		if len(kp.Encryption) > 0 {
			keyPairs = append(keyPairs, []byte(kp.Encryption))
		}
	}

	return keyPairs
}

// SetDefaults callback to force array generation
func (c *Cookie) SetDefaults() {
	kp := CookieKeyPair{}
	defaults.Set(&kp)
	c.Keys = []CookieKeyPair{kp}
}
