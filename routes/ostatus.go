package routes

import (
	"github.com/cking/reverse"
)

func init() {
	reverse.Add("ostatus-subscribe", "/subscribe")
}

// OStatusSubscribe returns the ostatus subscribe endpoint
func OStatusSubscribe() string {
	return reverse.Rev("ostatus-subscribe")
}
