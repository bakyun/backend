package middleware

import (
	"bytes"
	"encoding/base64"
	"encoding/gob"
	"strconv"

	"github.com/RangelReale/osin"
	"github.com/go-pg/pg"
	radix "github.com/mediocregopher/radix.v3"
	"github.com/rs/zerolog/log"
	"gitlab.com/paars/bakyun/backend/config"
	"gitlab.com/paars/bakyun/backend/internal/model"
	"gitlab.com/paars/bakyun/redix"
)

// OAuthStorage is the applications oauth storage provider
type OAuthStorage struct {
	db    *pg.DB
	redix *redix.Redix
}

// NewOAuthStorage creates a new oauth storage provider
func NewOAuthStorage(cfg *config.Config) *OAuthStorage {
	return &OAuthStorage{
		db:    nil,
		redix: nil,
	}
}

// Clone returns the current storage instance (no cloning nec)
func (oas *OAuthStorage) Clone() osin.Storage {
	return oas
}

// Close closes the storage instance (no-op)
func (oas *OAuthStorage) Close() {
	// dont
}

// GetClient retrieves an application description by a client id
func (oas *OAuthStorage) GetClient(id string) (osin.Client, error) {
	// return early for empty id
	if id == "" {
		return nil, osin.ErrNotFound
	}

	m, err := model.ApplicationByClientID(oas.db, id)
	if err != nil {
		if err != pg.ErrNoRows {
			log.Error().Err(err).Msg("failed to query for application")
		}

		return nil, osin.ErrNotFound
	}

	return m, nil
}

// SaveAuthorize saves the authorize metadata to the database
func (oas *OAuthStorage) SaveAuthorize(ad *osin.AuthorizeData) error {
	b := new(bytes.Buffer)
	err := gob.NewEncoder(b).Encode(ad)
	if err != nil {
		return err
	}

	key := "oauth:" + ad.Code
	value := base64.StdEncoding.EncodeToString(b.Bytes())

	p := radix.Pipeline(
		radix.Cmd(nil, "MULTI"),
		radix.Cmd(nil, "SET", key, value),
		radix.Cmd(nil, "EXPIRE", key, strconv.Itoa(int(ad.ExpiresIn))),
		radix.Cmd(nil, "EXEC"),
	)

	return oas.redix.Do(p)
}

// LoadAuthorize loads authorize metadata
func (oas *OAuthStorage) LoadAuthorize(code string) (*osin.AuthorizeData, error) {
	encodedGob, err := oas.redix.Strings().Get_String("oauth:" + code)
	if err != nil {
		return nil, err
	}
	serialized, err := base64.StdEncoding.DecodeString(encodedGob)
	if err != nil {
		return nil, err
	}
	serializedBuffer := bytes.NewBuffer(serialized)

	var ad *osin.AuthorizeData
	err = gob.NewDecoder(serializedBuffer).Decode(ad)
	if err != nil {
		return nil, err
	}

	return ad, nil
}

// RemoveAuthorize removes authoize metadata
func (oas *OAuthStorage) RemoveAuthorize(code string) error {
	_, err := oas.redix.Keys().Del("oauth:" + code)
	return err
}

func (oas *OAuthStorage) SaveAccess(*osin.AccessData) error {
	panic("not implemented")
}

func (oas *OAuthStorage) LoadAccess(token string) (*osin.AccessData, error) {
	panic("not implemented")
}

func (oas *OAuthStorage) RemoveAccess(token string) error {
	panic("not implemented")
}

func (oas *OAuthStorage) LoadRefresh(token string) (*osin.AccessData, error) {
	panic("not implemented")
}

func (oas *OAuthStorage) RemoveRefresh(token string) error {
	panic("not implemented")
}
